Source: mumble
Section: sound
Priority: optional
Maintainer: Christopher Knadle <Chris.Knadle@coredump.us>
Build-Depends: debhelper (>= 11),
 po-debconf,
 libboost-dev (>= 1.42),
 qtbase5-dev,
 qttools5-dev-tools,
 libqt5svg5-dev,
 libqt5opengl5-dev,
 libgl1-mesa-dev,
 libasound2-dev,
 libpulse-dev,
 libogg-dev,
 libspeex-dev,
 libspeexdsp-dev,
 libopus-dev,
 libsndfile1-dev,
 libssl-dev,
 libzeroc-ice-dev,
 libspeechd-dev,
 protobuf-compiler,
 libprotobuf-dev,
 libavahi-compat-libdnssd-dev,
 libxi-dev,
 libcap-dev [linux-any],
 libjack-jackd2-dev
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/pkg-voip-team/mumble
Vcs-Git: https://salsa.debian.org/pkg-voip-team/mumble.git
Homepage: https://wiki.mumble.info/wiki/Main_Page

Package: mumble
Architecture: any
Depends: ${shlibs:Depends},
 ${misc:Depends},
 libqt5sql5-sqlite,
 lsb-release
Suggests: mumble-server,
 speech-dispatcher
Description: Low latency encrypted VoIP client
 Mumble is a low-latency, high quality voice chat program for gaming.
 It features noise suppression, encrypted connections for both voice
 and instant messaging, automatic gain control and low latency audio
 with support for multiple audio standards. Mumble includes an in-game
 overlay compatible with most open-source and commercial 3D applications.
 Mumble is just a client and uses a non-standard protocol. You will need
 a dedicated server to talk to other users. Server functionality is
 provided by the package "mumble-server".

Package: mumble-server
Architecture: any
Depends: ${shlibs:Depends},
 ${misc:Depends},
 adduser,
 libqt5sql5-sqlite,
 lsb-base (>= 3.0-6)
Description: Low latency encrypted VoIP server
 Murmur is the VoIP server component for Mumble. Murmur is installed
 in a system-wide fashion, but can also be run by individual users.
 Each murmur process supports multiple virtual servers, each with their
 own user base and channel list.
